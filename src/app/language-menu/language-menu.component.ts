import { Component, OnInit } from '@angular/core';
import { LanguageService } from './language.service';
import { Language } from './language.module';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-language-menu',
  templateUrl: './language-menu.component.html',
  styleUrls: ['./language-menu.component.css']
})
export class LanguageMenuComponent implements OnInit {

 languagesArr:Language[]=[];
 chooseLanguage:number=0;
  constructor(private languageServe:LanguageService) { }

  ngOnInit() {
    //get all language from the service
    this.languageServe.getAllLanguages().subscribe(res => {
      this.languagesArr=res;
    });
  }

  chooseLanguageFunc(language:number)
  {
    //updte the chosen language in the language service
    this.chooseLanguage=language;
    this.languageServe.currentLanguageId.emit(language);
  }
}
