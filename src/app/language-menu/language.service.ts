import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Language } from './language.module';
import { Observable } from 'rxjs';
import { EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LanguageService {

 currentLanguageId=new EventEmitter<number>();

  constructor(private http:HttpClient) { }

  //get all languages
  getAllLanguages():Observable<Language[]>
  {
    return this.http.get<Language[]>("http://localhost:52137/api/Language");
  }
}
