import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Lecturer} from './lecturer.module';
import { lecturerDetails} from './lecturerDetails.module';
import { EventEmitter } from '@angular/core';


export class addNewlecturer{
 
  constructor(public id:number,public degree:string,public educational_institution:string,public user_mail:string,public expertise_areas:string,public comments:string)
  {
      
  }

}

@Injectable({
  providedIn: 'root'
})
export class LecturerService {

  lecturerFind = new EventEmitter<Lecturer>();

  constructor(private http:HttpClient) { }

  //get lecturer by id
  getLecturerById(id:number):Observable<Lecturer>
  {
    return this.http.get<Lecturer>("http://localhost:52137/api/Lecturer/"+id);
  }

  //get all lecturers
  getLecturers():Observable<lecturerDetails[]>
  {
    return this.http.get<lecturerDetails[]>("http://localhost:52137/api/Lecturer");
  }

  //get user details in order to register asa lecturer
  registerAsLecturer(values:addNewlecturer):Observable<boolean>
  {
    
    return this.http.post<boolean>("http://localhost:52137/api/Lecturer",values);
  }

  
}


