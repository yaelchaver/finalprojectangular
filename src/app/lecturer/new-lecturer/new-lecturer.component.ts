import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { LecturerService } from '../lecturer.service';


@Component({
  selector: 'app-new-lecturer',
  templateUrl: './new-lecturer.component.html',
  styleUrls: ['./new-lecturer.component.css']
})
export class NewLecturerComponent implements OnInit {

  newLecturer = new FormGroup({
    id: new FormControl(),
    degree: new FormControl(),
    educational_institution: new FormControl(),
    user_mail: new FormControl(),
    expertise_areas: new FormControl(),
    comments: new FormControl()
  });

  constructor(private lecturerServ:LecturerService) { }

  ngOnInit() {
  }

  Submit() {
    this.lecturerServ.registerAsLecturer(this.newLecturer.value)
      .subscribe(res => {});
  }

  email = new FormControl('', [Validators.required, Validators.email]);

  getErrorMessage() {
    if (this.email.hasError('required')) {
      return 'You must enter a value';
    }

    return this.email.hasError('email') ? 'Not a valid email' : '';

  }
}
