import { Component, OnInit } from '@angular/core';
import { LecturerService } from '../lecturer.service';
import { FormGroup, FormControl } from '@angular/forms';
import { UserService } from 'src/app/user/user.service';
import { User } from 'src/app/user/user.module';
import { lecturerDetails } from '../lecturerDetails.module';
import { Lecturer } from '../lecturer.module';
import { MatButtonToggleGroup, MatButtonToggle } from '@angular/material';

@Component({
  selector: 'app-management-lecturers',
  templateUrl: './management-lecturers.component.html',
  styleUrls: ['./management-lecturers.component.css']
})
export class ManagementLecturersComponent implements OnInit {

  lecturerArr: lecturerDetails[] = [];
  usersArr: User[] = [];
  add:boolean=true;

  managementLecturersForm = new FormGroup({
    lecturer: new FormControl(),
    user: new FormControl()
  });


  constructor(private lecturerServ: LecturerService, private userServe: UserService) { }

  ngOnInit() {
    this.lecturerServ.getLecturers()
      .subscribe(x => {
        this.lecturerArr = x;
      });

    // this.userServe.getWithoutLecturers()
    //   .subscribe(res => {
    //     this.usersArr = res;
    //   });  
  }

  Submit()
  {
    //console.log(MatButtonToggleGroup);
    //if(MatButtonToggle.prototype.value=='remove')
  }
}
