import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpRequest, HttpHeaders } from '@angular/common/http';
import { ViewChild, ElementRef } from '@angular/core';
import { HttpEventType, HttpErrorResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
//import { TrinigService } from  './trinig.service';

@Component({
  selector: 'app-trying',
  templateUrl: './trying.component.html',
  styleUrls: ['./trying.component.css']
})
export class TryingComponent {
  /**
   *
   */
  title = 'tests';


  constructor(private http: HttpClient) { }

  onFileChange(event: any) {
    console.log("file changed!!");
    let fi = event.srcElement;
    if (fi.files && fi.files[0]) {
      let fileToUpload = fi.files[0];
      let formData: FormData = new FormData();
      formData.append(fileToUpload.name, fileToUpload);
      let headers = new HttpHeaders({
        'Accept': 'application/json'
      });
      let options = {
        headers: headers
      }

      this.http.post("http://localhost:52137/api/Uploead", formData, options)
        .subscribe(r => console.log(r));
    }
  }







}


