import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserComponent } from './user/user.component';
import { HeaderComponent } from './header/header.component';
import {NgForm,ReactiveFormsModule, FormsModule} from '@angular/forms';
import { from } from 'rxjs';
import { NewUserComponent } from './user/new-user/new-user.component';
import { HttpClientModule } from '@angular/common/http';
import { ContainerComponent } from './container/container.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MaterialModule } from './material.module';
import { MenuComponent } from './menu/menu.component';
import { PrimeNgModule } from './primeNG.module';
import { LanguageMenuComponent } from './language-menu/language-menu.component';
import { SubjectMenuComponent } from './subject-menu/subject-menu.component';
import { TryingComponent } from './trying/trying.component';
import { FeedbackComponent} from './feedback/feedback.component'
import {DatePipe} from '@angular/common';
import { AddCourseComponent } from './course/add-course/add-course.component';
import { AddLessonComponent } from './course/add-lesson/add-lesson.component';
import { NewLecturerComponent } from './lecturer/new-lecturer/new-lecturer.component';
import { ManagementLecturersComponent } from './lecturer/management-lecturers/management-lecturers.component';
import { CoursesMenuComponent } from './courses-menu/courses-menu.component';
import { HelpComponent } from './help/help.component';




@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    HeaderComponent,
    NewUserComponent,
    ContainerComponent,
    MenuComponent,
    LanguageMenuComponent,
    SubjectMenuComponent,
    TryingComponent,
    FeedbackComponent,
    AddCourseComponent,
    AddLessonComponent,
    NewLecturerComponent,
    ManagementLecturersComponent,
    CoursesMenuComponent,
    HelpComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MaterialModule,
    PrimeNgModule
  ],
  providers: [DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
