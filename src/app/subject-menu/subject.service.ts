import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import {Subject} from './Subject.module';

@Injectable({
  providedIn: 'root'
})
export class SubjectService {
  subId:boolean=true;
  constructor(private http:HttpClient) {
    
   }

   //get all subjects list
   getAllSubjects():Observable<Subject[]>
   {
     return this.http.get<Subject[]>("http://localhost:52137/api/Subject");
   }
   
  
   //get subject by subject id
   getSubjectById(id:number):Observable<Subject>
   {
    return this.http.get<Subject>(`http://localhost:52137/api/Subject?subId=${id}`)
   }

   //get subjects by language id
   getByLanguageId(languageId:number):Observable<Subject[]>
   {
     return this.http.get<Subject[]>(`http://localhost:52137/api/Subject?id=${languageId}`)
   }

}
