import { Course } from '../course/course.module';

export class Subject
{
    constructor(public subject_id:number, public subject_name:string,public parent_id:number,public language_id:number,public subjectsArr: Subject[]=null,public coursesArr: Course[]=null)
    {

    }
}