import { NgModule } from '@angular/core';
import { MatBadgeModule } from '@angular/material/badge';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatChipsModule } from '@angular/material/chips';
import { MatButtonModule } from '@angular/material/button';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material';
import {MatTreeModule} from '@angular/material/tree';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatSliderModule} from '@angular/material/slider';
import {MatSelectModule} from '@angular/material/select';
import { MatToolbarModule, MatCardModule} from '@angular/material';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatListModule} from '@angular/material/list';
// import {MatTreeFlatDataSource, MatTreeFlattener} from '@angular/material/tree';
import { from } from 'rxjs';


@NgModule({
    imports: [
        MatBadgeModule,
        MatIconModule,
        MatFormFieldModule,
        MatInputModule,
        MatChipsModule,
        MatButtonModule,
        MatDatepickerModule,
        MatButtonModule,
        MatFormFieldModule,
        MatNativeDateModule,
        MatTreeModule,
        MatProgressBarModule,
        MatSliderModule,
        MatSelectModule,
        MatToolbarModule, 
        MatCardModule,
        MatSlideToggleModule,
        MatButtonToggleModule,
        MatListModule
        // FlatTreeControl,
        // MatTreeFlatDataSource, 
        // MatTreeFlattener
    ],
    exports: [
        MatBadgeModule,
        MatIconModule,
        MatFormFieldModule,
        MatInputModule,
        MatChipsModule,
        MatButtonModule,
        MatDatepickerModule,
        MatDatepickerModule,
        MatButtonModule,
        MatFormFieldModule,
        MatNativeDateModule,
        MatTreeModule,
        MatProgressBarModule,
        MatSliderModule,
        MatSelectModule,
        MatToolbarModule, 
        MatCardModule,
        MatSlideToggleModule,
        MatButtonToggleModule,
        MatListModule
        
        // FlatTreeControl,
        // MatTreeFlatDataSource, 
        // MatTreeFlattener
    ]
})
export class MaterialModule { }












