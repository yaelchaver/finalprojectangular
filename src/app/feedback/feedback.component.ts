import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { UserService } from '../user/user.service';
import { User } from '../user/user.module';
import { CanActivate } from '@angular/router';
import { CourseService } from '../course/course.service';
import { Course } from '../course/course.module';
import { SubjectService } from '../subject-menu/subject.service';
import { Subject } from '../subject-menu/Subject.module';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.css']
})
export class FeedbackComponent implements OnInit {

  courses: Course[] = [];
  courseName: string[] = ["course1","course2"];
  temp: Subject;

  addFeedback: FormGroup = new FormGroup({
    course_id: new FormControl(),
    mark: new FormControl(),
    comment: new FormControl()
  });
  constructor(private courseServ: CourseService, private subjectServ: SubjectService) { }


  ngOnInit() {
    //I need to continue do it.....


    // this.courseServ.getAllCourses().subscribe(res => {
     
    //   this.courses = res;
    //   this.courses.forEach(element => {
    
    //     this.subjectServ.getSubjectById(element.subject_id).subscribe(res2 => {
    //       this.temp = res2;
    //       console.log(element.subject_id);
    //       console.log(res2.subject_name);
    //       this.courseName.push(res2.subject_name);
    //     });
    //   });
    // });
  }



  formatLabel(value: number) {
    return Math.round(value / 1000);
  }

}
