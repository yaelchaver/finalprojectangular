import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, UrlTree } from '@angular/router';
import { UserService } from './user/user.service';
import { User } from './user/user.module';
import { AuthService }      from './auth/auth.service';

@Injectable({
    providedIn: 'root',
  })
export class MyGuard implements CanActivate {
    constructor(private userServ: UserService,private authService: AuthService, private router: Router) {

    }
    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): true|UrlTree {
        let url: string = state.url;
    
        return this.checkLogin(url);
      }
    
      checkLogin(url: string): true|UrlTree {
        if (this.authService.isLoggedIn) { return true; }
    
        // Store the attempted URL for redirecting
        this.authService.redirectUrl = url;
    
        // Redirect to the login page
        return this.router.parseUrl('/login');
      }

    // canActivate() {

    //     if (this.userServ.userFind != null)
    //         return true;
    //     return false;
    // }
}