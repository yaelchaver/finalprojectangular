import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewUserComponent } from './user/new-user/new-user.component';
import {UserComponent} from './user/user.component'
import { ContainerComponent } from './container/container.component';
import {LanguageMenuComponent} from './language-menu/language-menu.component';
import { FeedbackComponent } from './feedback/feedback.component';
import { AddCourseComponent } from './course/add-course/add-course.component';
import { MyGuard } from './my-guard'
import { from } from 'rxjs';
import { AddLessonComponent } from './course/add-lesson/add-lesson.component';
import { NewLecturerComponent } from './lecturer/new-lecturer/new-lecturer.component';
import { ManagementLecturersComponent } from './lecturer/management-lecturers/management-lecturers.component';



const routes: Routes = [
  {path:'register_as_lecturer',component:NewLecturerComponent,canActivate:[MyGuard]},
  {path:'management_lecturers',component:ManagementLecturersComponent,canActivate:[MyGuard]},
  { path:'signUp', component:NewUserComponent },
  { path:"login", component:UserComponent },
  { path:"edit/:id", component:NewUserComponent,canActivate:[MyGuard] },
{path:"add-course",component:AddCourseComponent,canActivate:[MyGuard]},
{path:"add-lesson",component:AddLessonComponent,canActivate:[MyGuard]},
  { path:"language-menu", component:LanguageMenuComponent,canActivate:[MyGuard] },
  {path:"feedback",component:FeedbackComponent,canActivate:[MyGuard]},
  { path:"**", redirectTo:"login" }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
