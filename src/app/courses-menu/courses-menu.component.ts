import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Course } from '../course/course.module';
import { SubjectService } from '../subject-menu/subject.service';
import { Subject } from '../subject-menu/Subject.module';
import { CourseService } from '../course/course.service';
import { Lecturer } from '../lecturer/lecturer.module';
import { LecturerService } from '../lecturer/lecturer.service';
import { UserService } from '../user/user.service';
import { User } from '../user/user.module';

export class courseAndLecturer{
  constructor(public course:Course,public lecturer: User)
  {
    
  }
}

@Component({
  selector: 'app-courses-menu',
  templateUrl: './courses-menu.component.html',
  styleUrls: ['./courses-menu.component.css']
})


export class CoursesMenuComponent implements OnInit {
  @Input() curSubjsctId = new EventEmitter<number>();
  curSubject: Subject;
  courses:courseAndLecturer[]=[];
  temp:courseAndLecturer;

  constructor(private subjectServ: SubjectService, private courseServ: CourseService,private userServ:UserService) { }

  ngOnInit() {
      this.curSubjsctId.subscribe(res => {
        this.courses=[];
        this.courseServ.getCourseBySubId(res).subscribe(res2 => {
          res2.forEach(element => {
           this.userServ.getUserById(element.lecturer_id).subscribe(res3 => {
            this.temp=new courseAndLecturer(element,res3)
            this.courses.push(this.temp);
            });
          });
        });
      });
  }

}
