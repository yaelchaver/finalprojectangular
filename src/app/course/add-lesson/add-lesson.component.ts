import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-add-lesson',
  templateUrl: './add-lesson.component.html',
  styleUrls: ['./add-lesson.component.css']
})
export class AddLessonComponent implements OnInit {
  @Input() id;
  
  constructor() { }

  ngOnInit() {
  }

}
