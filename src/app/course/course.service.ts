import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Course } from './course.module';

@Injectable({
  providedIn: 'root'
})
export class CourseService {

  constructor(private http:HttpClient) {}
  
  getAllCourses():Observable<Course[]>
  {
    return this.http.get<Course[]>("http://localhost:52137/api/Course");
  }

  getCourseBySubId(subId:number):Observable<Course[]>
  {
    return this.http.get<Course[]>(`http://localhost:52137/api/Course?subId=${subId}`);
    
  }

}
