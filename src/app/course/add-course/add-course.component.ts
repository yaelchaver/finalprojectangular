import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { LanguageService } from 'src/app/language-menu/language.service';
import { Language } from 'src/app/language-menu/language.module';
import { Subject } from 'src/app/subject-menu/Subject.module';
import { SubjectService } from 'src/app/subject-menu/subject.service';
import { Lesson } from 'src/app/lesson/lesson.module';
import { Course } from 'src/app/course/course.module'
import { from } from 'rxjs';
import { UserService } from 'src/app/user/user.service';
import { User } from 'src/app/user/user.module';

@Component({
  selector: 'app-add-course',
  templateUrl: './add-course.component.html',
  styleUrls: ['./add-course.component.css']
})


export class AddCourseComponent implements OnInit {

  languages: Language[] = [];
  subjects: Subject[] = [];
  lessons: Lesson[] = [];
  lesson_index: number = 1;
  Mycourse: Course;
  currentUser: User;
  selectLanguage: boolean = false;
  addSubject: boolean = false;
  s: Subject;

  addCourse = new FormGroup({
    course_name: new FormControl(),
    course_language: new FormControl(),
    subject_id: new FormControl()
  });

  addSubjectForm = new FormGroup({
    subject_name: new FormControl(),
    subSubject: new FormControl(),
    parent_id: new FormControl()
  });

  constructor(private languageServ: LanguageService, private subjectServ: SubjectService, private userServ: UserService) {
  }

  ngOnInit() {
    this.currentUser = this.userServ.getCurentUser();
    //implemnte the new lesson in lessons;
    this.lessons[0] = new Lesson(null, 1, null, null);

    //get all language from languageService
    this.languageServ.getAllLanguages().subscribe(res => {
      this.languages = res;
    });
  }

  //when the language selected- get all its subjects
  onLanguageChange(deviceValue) {
    this.selectLanguage = true;
    this.subjectServ.getByLanguageId(deviceValue).subscribe(res => {
      this.subjects = res;
      this.subjects.forEach(element => {
        element.subjectsArr.forEach(e => {
          this.subjects.push(e);
          this.recursiveSubjects(e);
        });
      });
    });
  }


  //recursive function to find the sub-subjects
  recursiveSubjects(s: Subject): void {
    if (s.subjectsArr.length > 0) {
      s.subjectsArr.forEach(e => {
        this.subjects.push(e);
        this.recursiveSubjects(e);
      });
    }
  }

  onSubjectChange(deviceValue_) {
    if (deviceValue_ == -1)
      this.addSubject = true;
    else
      this.addSubject = false;
  }


  addSubjectSubmit() {
    this.s = this.addSubjectForm.getRawValue()
    console.log(this.s);
    this.subjects.push(this.s);
    //this.addCourse.value.subject_id.selected(this.s);
    this.addSubject=false;
  }

  Submit(): void {

    //create new course
    this.Mycourse = new Course(this.addCourse.value.subject_id.value, this.currentUser.user_id, this.addCourse.value.course_name.value);
  }


  addLesson() {
    //add lesson form
    this.lessons[this.lesson_index] = new Lesson(null, ++this.lesson_index, null, null);
  }
}


