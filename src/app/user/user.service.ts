import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from './user.module';
import { EventEmitter } from '@angular/core';

export class uu {
 
  constructor(public firstName: string, public lastName: string,public  pass: string)
  {
      
  }

}

@Injectable({
  providedIn: 'root'
})
export class UserService {
users:User[];
flag:boolean=true;
private curentUSer:User;

userFind = new EventEmitter<User>();

  constructor(private http:HttpClient) { 
    this.userFind.subscribe(user=>{
      this.curentUSer=user;
    });
  }

  getCurentUser():User{
    return this.curentUSer;
  }
 
  getAllUsers():Observable<User[]>
  {
    return this.http.get<User[]>("http://localhost:52137/api/User");
  }

  // getWithoutLecturers():Observable<User[]>
  // {
  //   return this.http.get<User[]>(`http://localhost:52137/api/User/?flag=${this.flag}`);
  // }

  getLogin(values:uu):Observable<User>{
    return this.http.get<User>("http://localhost:52137/api/User",{params:{FN: values.firstName,LN: values.lastName,P:values.pass}});
  }

  getUserById(id:number):Observable<User>
  {
    return this.http.get<User>("http://localhost:52137/api/User/"+id);
  }

  editUser(user: User):Observable<User>
  {
    return this.http.post<User>("http://localhost:52137/api/User",user);
  }

  addUser(user: User):Observable<User>
  {
    return this.http.post<User>("http://localhost:52137/api/User",user);
  }
}

 


  

