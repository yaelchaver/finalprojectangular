import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,ParamMap, Router  } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { User } from '../user.module';
import { UserService } from '../user.service';
import { ViewEncapsulation} from '@angular/core';
import { MatCalendarCellCssClasses } from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material';



@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class NewUserComponent implements OnInit {

  isEdit: boolean=false;
  isHide: boolean = true;
  userId: any = null;
  currentUser: User = null;
  

  dateClass = (d: Date): MatCalendarCellCssClasses => {
    const date = d.getDate();
    return (date === 1 || date === 20) ? 'example-custom-date-class' : '';}

  createUser = new FormGroup({
    user_first_name: new FormControl(),
    user_last_name: new FormControl(),
    born_date: new FormControl(),
    user_password: new FormControl(),
    user_mail: new FormControl(),
  });

  constructor(private activeRoute: ActivatedRoute, private route: Router, private UserServe: UserService) { }

  ngOnInit() {
    //if its edit get user id and find the current user
    this.userId=this.activeRoute.snapshot.paramMap.get('id');
    this.UserServe.getUserById(this.userId).subscribe(res => {
       //keep the user in currentUser
      this.currentUser = res;
      //if the current user was found show his details
      if (this.currentUser) {
        this.isEdit=true;
        this.createUser.setValue({
          user_first_name:this.currentUser.user_first_name,
          user_last_name:this.currentUser.user_last_name,
          user_mail:this.currentUser.user_mail,
          user_password:this.currentUser.user_password,
          born_date:this.currentUser.born_date
      });
        
      }
    })
  }

  Submit() {
    //if its edit
    if (this.currentUser) {
      //to update user details in this component and on the user servise
      this.currentUser=this.createUser.getRawValue();
      this.currentUser.user_id=this.userId;
      this.UserServe.editUser(this.currentUser).subscribe(res=>{
        this.UserServe.userFind.emit(res);
      });
    }
    //if his new user
    else {
      //keep the user in currentUser
      this.currentUser=this.createUser.getRawValue();
      //add the new user to the database
      this.UserServe.addUser(this.currentUser).subscribe(res=>{
        this.UserServe.userFind.emit(res);
      });
    }

  }
  email = new FormControl('', [Validators.required, Validators.email]);

  getErrorMessage() {
    if (this.email.hasError('required')) {
      return 'You must enter a value';
    }

    return this.email.hasError('email') ? 'Not a valid email' : '';
  }
}
