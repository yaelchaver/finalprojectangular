import { Component, OnInit } from '@angular/core';
import { User } from './user.module';
import { FormGroup, FormControl } from '@angular/forms';
import { UserService } from './user.service';
import { Router } from '@angular/router';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})

export class UserComponent implements OnInit {

  //hide:boolean=true;
  submit: boolean = false;
  login: FormGroup = new FormGroup({
    firstName: new FormControl(),
    lastName: new FormControl(),
    pass: new FormControl()
  });


  constructor(private userServ: UserService, private router: Router) { }
  Submit() {
    if (!this.submit)
    {
      this.submit=true;
      //login
      this.userServ.getLogin(this.login.value)
        .subscribe(res => {
          //if there is suitable user
          if (res) {
            this.router.navigate(['language-menu']);
            this.userServ.userFind.emit(res);
          }
          else
            this.router.navigate(['signUp']);

        }, err => {
          this.router.navigate(['/signUp']);
        });
  }
}

ngOnInit() {
}

}



