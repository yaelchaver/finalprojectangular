import { Subject } from '../subject-menu/Subject.module';

export class Lesson{
    constructor(public course_id:number,public index:number,public title:string,public is_sample:boolean)
    {
        
    }
}