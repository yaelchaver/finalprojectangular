import { Component, OnInit } from '@angular/core';
import { UserService } from '../user/user.service';
import { User } from '../user/user.module';
import { LecturerService } from '../lecturer/lecturer.service';
import { Lecturer } from '../lecturer/lecturer.module';
import { AuthService } from '../auth/auth.service';
import { EventEmitter } from 'protractor';




@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})



export class HeaderComponent implements OnInit {

  login: boolean = false;
  currentUser: User;
  currLecturer: Lecturer;
  menu: any;

  constructor(private userService: UserService, private lecturerServise: LecturerService,private authoservice:AuthService) { }

//list of the menu options for student
  studentMenuList = [
    {
      title: 'תפריט שפות',
      link: 'language-menu'
    },
    {
      title: 'הסימניות שלי',
      link: 'dgdfgdg'
    },
    {
      title: 'חיפוש',
      link: 'dgdfgdg'
    },
    {
      title:'פידבק',
      link:'feedback'
    },
    {
      title:'הרשם כמרצה',
      link:'register_as_lecturer'
    }
  ]

  //list of the menu options for manager
  managerMenuList = [{
    title: 'לימוד שפות',
    link: 'language-menu'
  },
  {
    title: 'הוספת קורס',
    link: 'add-course'
  },
  {
    title: 'פידבקים',
    link: 'dgdfgdg'
  },
  {
    title: 'דוחות',
    link: 'dgdfgdg'
  },
  {
    title: 'נהול מרצים',
    link: 'management_lecturers'
  },
  {
    title: 'חיפוש',
    link: 'dgdfgdg'
  }]

  //list of the menu options for lecturer
  lecturerMenuList = [{
    title: 'לימוד שפות',
    link: 'language-menu'
  },
  {
    title: 'הוספת קורס',
    link: 'add-course'
  },
  {
    title: 'הפידבקים שלי',
    link: 'dgdfgdg'
  },
  {
    title: 'חיפוש',
    link: 'dgdfgdg'
  }]


  ngOnInit() {
    this.userService.userFind.subscribe((user: User) => {
      //when there is a registered user
      if (user !== null) {
        //update about login and keep the current user
        this.authoservice.firstlogin();
        this.currentUser = user;
        this.login = true;
        //if is manager update the menu list options
        if (this.currentUser.isManager) {
          this.managerMenuList.push({
            title: 'עדכן הפרופיל',
            link: `edit/${this.currentUser.user_id}`
          });
          this.menu = this.managerMenuList;
        }
        else {
          //check if the current user is lecturer
          this.lecturerServise.getLecturerById(this.currentUser.user_id).subscribe(res => {
            this.lecturerServise.lecturerFind.emit(res);
            if (res != null) {
              //if is lecturer update the menu list options
              this.lecturerMenuList.push({
                title: 'עדכן הפרופיל',
                link: `edit/${this.currentUser.user_id}`
              });
              this.menu = this.lecturerMenuList;
            } else
            {
              //update the menu list in student options
              this.studentMenuList.push({
                title: 'עדכן הפרופיל',
                link: `edit/${this.currentUser.user_id}`
              });
               this.menu = this.studentMenuList;
            } 
          });
        }
      }
    });
  }

  //logout
  logOut():void
  {
    this.login=false;
    this.userService.userFind.emit(null);
    this.authoservice.logout();
  }
}


